<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="la page des animaux description à voir">
    <title>LE BESTIAIRE</title>
    <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css" />
</head>
<body>

<?php get_header(); ?>

<?php
 if ( have_posts() ) :
     while ( have_posts() ) : the_post();?>

    <?php the_content(); ?>
    <?php wp_link_pages(); ?>
    <//?php edit_post_link(); ?>


    <P>TAILLE : <?php echo get_field("taille") ?></P>
    <P><?php echo get_field("description") ?></P>
    <h3>Mange</h3>
    
   <?php $mange = get_field("mange");

if($mange){

foreach($mange as $mangeur){ ?>

        <a href="<?php echo get_the_permalink($mangeur) ?>"> <?php echo get_the_title($mangeur)?></P>

<?php }}else{
           echo 'pas de proies';
        }
     ?>
   
   <?php   endwhile;
   endif;
   ?>

</body>
</html>
